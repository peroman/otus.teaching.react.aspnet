import React, { Component } from 'react';
import { Route } from 'react-router';
import  Weather  from './components/Weather';

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <>
        <Weather />
      </>
    );
  }
}